#!/usr/bin/env python3

# This was an example interview problem given by Amazon.  Find edges that when removed disconnect the graph.
# There's a Tarjan algorithm for this also that I haven't understood yet.

def dfs(node, visited, in_spanning_tree, connections, parent_edge):
    visited[node] = True
    for i, (a, b) in enumerate(connections):
        a, b = a-1, b-1
        if a == node:
            if not visited[b]:
                parent_edge[b] = i
                in_spanning_tree[i] = True
                dfs(b, visited, in_spanning_tree, connections, parent_edge)
        elif b == node:
            if not visited[a]:
                parent_edge[a] = i
                in_spanning_tree[i] = True
                dfs(a, visited, in_spanning_tree, connections, parent_edge)

def dfs_run(serversNum, connectionsNum, connections):
    in_spanning_tree = [False] * connectionsNum
    visited = [False] * serversNum
    parent_edge = [-1] * serversNum # every node has a parent edge
    dfs(0, visited, in_spanning_tree, connections, parent_edge)
    return in_spanning_tree, parent_edge

def mark_all(edge, connections, parent_edge, is_bridge):
    is_bridge[edge] = False
    parent = parent_edge[connections[edge][0]-1]
    if parent != -1 and is_bridge[parent] == True:
        mark_all(parent, connections, parent_edge, is_bridge)

    parent = parent_edge[connections[edge][1]-1]
    if parent != -1 and is_bridge[parent] == True:
        mark_all(parent, connections, parent_edge, is_bridge)

def findCriticalConn(serversNum, connectionsNum, connections):
    # Overall strategy:
    # 1. Find spanning tree
    # 2. Find edges not in spanning tree
    is_bridge = [True] * connectionsNum

    in_spanning_tree, parent_edge = dfs_run(serversNum, connectionsNum, connections)

    for i, is_in_tree in enumerate(in_spanning_tree):
        if is_in_tree == False:
            # 3. For both sides of the edge, mark edges as redundant
            mark_all(i, connections, parent_edge, is_bridge)

    # 4. scan for non-redundant edges
    for i, val in enumerate(is_bridge):
        if val:
            print(connections[i])

    pass

if __name__ == "__main__":
    findCriticalConn(4, 4, [[1, 2], [1, 3], [3, 2], [3, 4]])
    findCriticalConn(4, 4, [[1, 2], [3, 4], [1, 3], [3, 2], [3, 4], [3, 4], [3, 4], [3, 4]])
    findCriticalConn(4, 4, [[1, 2], [1, 3], [3, 2], [4, 3]])
    findCriticalConn(3, 3, [[1, 2], [1, 3], [3, 2]])
    findCriticalConn(3, 3, [[1, 2], [1, 3], [2, 3]])
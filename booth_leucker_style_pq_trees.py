
class PQTreeNode():
    def __init__(self, item):
        self.item = item
        self.parent = None
        self.imm_siblings = []
        self.mark = None

    def get(self):
        return self.item

    def immediate_siblings(self, mark = None):
        return self.imm_siblings

    def __repr__(self):
        return "BoothLeuckerAlgPQTreeNode(" + repr(self.item) + ")"

class PQTree(): # Booth & Leucker
    def __init__(self):
        self.root = BoothLeuckerAlgPQTreeNode(None)

    def solve(self, cs):
        for seq in cs.sequences:
            self.add_seq(seq)
        return self.traverse()

    def add_seq(self, seq):
        self.bubble(seq)
        self.reduce(seq)

    def bubble(self, seq):
        queue = MyQueue()
        block_count = 0
        blocked_nodes = 0
        off_the_top = 0
        for item in seq:
            queue.put(item)
        while len(queue) + block_count + off_the_top > 1:
            if len(queue) == 0:
                raise Exception("T = None!")
            x = queue.get()
            x.mark = "blocked"
            bs = x.immediate_siblings("blocked")
            us = x.immediate_siblings("unblocked")
            if len(us) > 0:
                y = us[0]
                x.parent = y.parent
                x.mark = "unblocked"
            elif len(x.immediate_siblings()) < 2:
                x.mark = "unblocked"
            if x.mark == "unblocked":
                y = x.parent
                lst = []
                if len(bs) > 0:
                    lst = self.get_consecutive_blocked(x)
                    for z in list:
                        z.mark = "unblocked"
                        z.parent = y
                        y.pertinent_child_count += 1
                if y is None:
                    off_the_top = 1
                else:
                    y.pertinent_child_count += 1
                    if y.mark == "unmarked":
                        queue.put(y)
                        y.mark = "queued"
                block_count -= len(bs)
                blocked_nodes -= len(lst)
            else:
                block_count += 1 - len(bs)
                blocked_nodes += 1

    def reduce(self, seq):
        queue = MyQueue()
        for item in seq:
            queue.put(item)
            item.pertinent_leaf_count = 1
        while len(queue) > 0:
            x = queue.get()
            if x.pertinent_leaf_count < len(seq):
                # x is not root(t, s)
                y = x.parent
                y.pertinent_leaf_count += x.pertinent_leaf_count
                y.pertinent_child_count -= 1
                if y.pertinent_leaf_count == 0:
                    queue.put(y)
                if not template_l1(x):
                    if not template_p1(x):
                        if not template_p3(x):
                            if not template_p5(x):
                                if not template_q1(x):
                                    if not template_q2(x):
                                        raise Exception("T = None!")
            else:
                # x is root(t, s)
                if not template_l1(x):
                    if not template_p1(x):
                        if not template_p2(x):
                            if not template_p4(x):
                                if not template_p6(x):
                                    if not template_q1(x):
                                        if not template_q2(x):
                                            if not template_q3(x):
                                                raise Exception("T = None!")



    def traverse(self):
        pass

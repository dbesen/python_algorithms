#!/usr/bin/env python

import unittest
from linked_list import LinkedList

def get_digit(num, digit):
    return num // 10**digit % 10

def max_len(ints):
    m = 0
    for i in ints:
        l = len(str(i))
        if l > m:
            m = l
    return m

def integer_radix_sort(ints):
    num_passes = max_len(ints)
    num_buckets = 10 # since we're dealing with integers


    for digitpos in range(0, num_passes):
        # Reset buckets
        buckets = []
        for i in range(num_buckets):
            buckets.append(LinkedList())
        # Drop ints in buckets
        for i in ints:
            digit = get_digit(i, digitpos)
            buckets[digit].append(i)

        # Rebuild ints
        ints = []
        for b in buckets:
            ints.extend(b.traverse())

    return ints


class test_radix(unittest.TestCase):
    def test_get_digit(self):
        self.assertEqual(0, get_digit(210, 0))
        self.assertEqual(1, get_digit(210, 1))
        self.assertEqual(2, get_digit(210, 2))
        self.assertEqual(1, get_digit(1234, 3))
        self.assertEqual(0, get_digit(1234, 8))

    def test_max_len(self):
        self.assertEqual(0, max_len([]))
        self.assertEqual(1, max_len([1]))
        self.assertEqual(2, max_len([1, 23]))
        self.assertEqual(3, max_len([234, 1, 23]))

    def test_integer_radix_sort(self):
        self.assertEqual([0, 1], integer_radix_sort([0, 1]))
        self.assertEqual([0, 1], integer_radix_sort([1, 0]))
        self.assertEqual([10, 11], integer_radix_sort([11, 10]))
        self.assertEqual([10, 20], integer_radix_sort([20, 10]))
        self.assertEqual(list(range(100)), integer_radix_sort(range(99, -1, -1)))

    def test_original_list_not_modified(self):
        l = [1, 0]
        integer_radix_sort(l)
        self.assertEqual([1, 0], l)

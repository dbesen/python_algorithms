#!/usr/bin/env python

import unittest

class Node(object):
    def __init__(self, data):
        self.next = None
        self.data = data

class LinkedList(object):
    def __init__(self):
        dummy = Node(None)
        self.headptr = dummy
        self.tailptr = dummy

    def append(self, item):
        n = Node(item)
        self.tailptr.next = n
        self.tailptr = n

    def traverse(self):
        ptr = self.headptr.next # dummy
        while ptr is not None:
            yield ptr.data
            ptr = ptr.next

    def __repr__(self):
        return "LinkedList(" + str(list(self.traverse())) + ")"

class TestLinkedList(unittest.TestCase):
    def test_empty(self):
        l = LinkedList()
        self.assertEquals([], list(l.traverse()))

    def assert_linkedlist(self, ints):
        l = LinkedList()
        for i in ints:
            l.append(i)

        result = list(l.traverse())
        self.assertEqual(ints, result)

    def test_linkedlist(self):
        self.assert_linkedlist([])
        self.assert_linkedlist([3])
        self.assert_linkedlist([3, 4, 5])

#!/usr/bin/env python3

class DisjointSet:
    def __init__(self, initial_value):
        self.val = initial_value
        self.parent = None
        self.rank = 0

    def findRoot(self):
        p = self
        while p.parent != None:
            p = p.parent
            self.parent = p # Path compression
        return p

    def union(self, other):

        parent_root = self.findRoot()
        child_root = other.findRoot()

        if parent_root == child_root:
            return

        # figure out which should be the child, by rank
        if parent_root.rank < child_root.rank:
            parent_root, child_root = child_root, parent_root

        child_root.parent = parent_root
        if parent_root.rank == child_root.rank:
            parent_root.rank += 1

import unittest

class TestDisjointSet(unittest.TestCase):
    def test_self_root(self):
        d = DisjointSet(3)
        self.assertEqual(d, d.findRoot())

    def test_unnecessary_union(self):
        d = DisjointSet(3)
        d.union(d)
        self.assertEqual(d, d.findRoot())

    def test_union(self):
        d = DisjointSet(3)
        d2 = DisjointSet(2)
        d.union(d2)
        self.assertEqual(d.findRoot(), d2.findRoot())


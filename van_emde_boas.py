
import unittest
import math

class TestVanEmdeBoas(unittest.TestCase):

    def assert_high_low(self, size, val, exp_high, exp_low):
        v = VanEmdeBoas(size)
        high = v.high(val)
        self.assertEquals(exp_high, high)
        low = v.low(val)
        self.assertEquals(exp_low, low)

    def test_high_low(self):
        self.assert_high_low(16, 9, 2, 1)
        self.assert_high_low(16, 1, 0, 1)

    def test_successor_empty(self):
        v = VanEmdeBoas(16)
        self.assertEqual(None, v.successor(3))

    def test_successor_simple(self):
        v = VanEmdeBoas(16)
        v.insert(3)
        self.assertEqual(3, v.successor(2))

    def test_successor_same(self):
        v = VanEmdeBoas(16)
        v.insert(3)
        self.assertEqual(None, v.successor(3))

    def test_successor_several(self):
        v = VanEmdeBoas(16)
        v.insert(3)
        v.insert(5)
        v.insert(7)
        self.assertEqual(5, v.successor(4))

    def test_successor_across_empty(self):
        v = VanEmdeBoas(16)
        v.insert(15)
        self.assertEqual(15, v.successor(1))

    def test_successor_across_empty_exists(self):
        v = VanEmdeBoas(16)
        v.insert(1)
        v.insert(15)
        self.assertEqual(15, v.successor(1))

class SizeOneVanEmdeBoas(object):
    def __init__(self):
        self.value = None

    def insert(self, i):
        self.value = i

    def successor(self, i):
        if i < self.value:
            return self.value
        return None

    def delete(self, i):
        if i != self.value:
            raise Exception("Item not found in delete from size-one tree")
        self.value = False

class VanEmdeBoas(object):
    def __init__(self, universe_size):
        self.min = None
        self.max = None
        self.universe_size = universe_size
        self.sqrt_size = int(math.sqrt(universe_size))

        if self.sqrt_size == 1:
            self.summary_table = None
            self.tables = [SizeOneVanEmdeBoas() for i in range(self.sqrt_size)]
        else:
            self.summary_table = VanEmdeBoas(self.sqrt_size)
            # TODO: don't create empty tables until they're needed
            self.tables = [VanEmdeBoas(self.sqrt_size) for i in range(self.sqrt_size)]

    def high(self, i):
        return i // self.sqrt_size

    def low(self, i):
        return i % self.sqrt_size

    def index(self, high, low):
        return low + high * self.sqrt_size

    def insert(self, i):
        if self.max == None or self.max < i:
            self.max = i
        if self.min == None:
            self.min = i
            return
        if i < self.min:
            (self.min, i) = (i, self.min)
        high = self.high(i)

        # Insert into summary table if needed
        if self.tables[high].min == None:
            self.summary_table.insert(high)

        self.tables[high].insert(self.low(i))

    def successor(self, i):
        if self.min == None:
            return None
        if i < self.min:
            return self.min
        if i >= self.max:
            return None
        high = self.high(i)
        # Which table is i in?
        s = self.tables[high].successor(self.low(i))
        if s is not None:
            return self.index(high, s)
        # Which table is the successor in?
        s = self.summary_table.successor(high)
        return self.index(s, self.tables[s].min)


    def delete(self, i):
        pass # TODO

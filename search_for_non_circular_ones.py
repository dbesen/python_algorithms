import pq_trees
import random
import unittest

class TestSomething(unittest.TestCase):
    def test_find_first_non_circular_ones_row_but_is_circular(self):
        # This matrix returned non-None in the past.
        matrix = [[0, 0, 0, 1, 1],
                  [0, 1, 1, 0, 1],
                  [0, 0, 1, 0, 1],
                  [0, 1, 1, 0, 0],
                  [0, 0, 1, 1, 1]]

        matrix = Matrix(matrix)
        self.assertIsNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_is_circular_2(self):
        # Another actual failure.
        matrix = [[0, 1, 1, 1, 0],
                  [0, 1, 0, 1, 0],
                  [0, 0, 0, 0, 0],
                  [0, 1, 1, 1, 1]
                 ]

        matrix = Matrix(matrix)
        self.assertIsNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_is_circular_3(self):
        # Another actual failure.
        matrix = [[0, 1, 1, 1, 0, 1],
                  [0, 0, 1, 1, 1, 1],
                  [0, 0, 0, 1, 0, 1],
                  [0, 1, 1, 1, 0, 1]
                 ]

        matrix = Matrix(matrix)
        self.assertIsNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_is_circular_4(self):
        matrix = [[0, 1, 0, 0, 0, 0, 1],
                  [0, 0, 1, 0, 0, 0, 1],
                  [0, 1, 1, 1, 0, 0, 1],
                  [0, 0, 0, 0, 0, 0, 1],
                  [0, 1, 1, 1, 0, 1, 1],
                  [0, 0, 0, 1, 1, 0, 0]
                 ]

        matrix = Matrix(matrix)
        self.assertIsNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_is_circular_5(self):
        matrix = [[0, 0, 1, 1, 1, 0, 1, 1],
                  [0, 0, 0, 1, 0, 0, 1, 0],
                  [0, 0, 1, 0, 1, 0, 0, 0],
                  [0, 0, 1, 0, 0, 0, 0, 1],
                  [0, 0, 1, 1, 1, 0, 0, 1],
                  [0, 1, 1, 1, 1, 1, 0, 1],
                  [0, 0, 0, 0, 1, 1, 0, 0]
                 ]

        matrix = Matrix(matrix)
        self.assertIsNone(matrix.find_first_non_circular_ones_row())


    def test_find_first_non_circular_ones_row(self):
        # This matrix returned None in the past.  It's not circular.
        matrix = [[0, 0, 1, 1, 1],
                  [0, 1, 0, 0, 1],
                  [0, 0, 0, 1, 1],
                  [0, 1, 1, 0, 1],
                 ]


        matrix = Matrix(matrix)
        self.assertIsNotNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_ones_row_2(self):
        matrix = [[0, 0, 1, 0, 1],
                  [0, 1, 1, 0, 1],
                  [0, 1, 1, 1, 0],
                  [0, 1, 0, 0, 1],
                  [0, 0, 0, 0, 1]
                 ]

        matrix = Matrix(matrix)
        self.assertIsNotNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_ones_row_3(self):
        matrix = [[0, 0, 1, 0, 1, 0],
                  [0, 0, 1, 1, 0, 0],
                  [0, 1, 1, 0, 1, 1],
                  [0, 0, 1, 0, 0, 1],
                  [0, 1, 0, 0, 0, 1]
                 ]
        matrix = Matrix(matrix)
        self.assertIsNotNone(matrix.find_first_non_circular_ones_row())

    def test_find_first_non_circular_ones_row_4(self):
        # This one failed intermittently.  If I run it a few times it seems to fail reliably.
        for i in range(0, 20):
            matrix = [[0, 1, 1, 0, 1, 1],
                      [0, 1, 1, 0, 0, 0],
                      [0, 0, 1, 1, 0, 1],
                      [0, 0, 0, 1, 0, 1],
                      [0, 0, 1, 1, 1, 1]
                     ]
            matrix = Matrix(matrix)
            self.assertIsNotNone(matrix.find_first_non_circular_ones_row())


def inverse_row(row):
    ret = []
    for item in row:
        if item == 0:
            ret.append(1)
        else:
            ret.append(0)
    return ret

class Matrix():
    def __init__(self, data):
        self.data = data

    def num_rows(self):
        return len(self.data)

    def column_with_least_ones(self):
        return 0 # TODO

    def remove_row(self, row):
        del self.data[row]

    def add_row(self, row):
        self.data.append(row)

    def has_row(self, test_row):
        for row in self.data:
            has = True
            for col in range(0, len(row)):
                if test_row[col] != row[col]:
                    has = False
                    break
            if has == True:
                return True
        return False

    def has_row_or_inverse(self, test_row):
        if self.has_row(test_row):
            return True
        if self.has_row(inverse_row(test_row)):
            return True
        return False

    def rows_that_have_a_one_in_col(self, col):
        ret = []
        for i in range(len(self.data)):
            if self.data[i][col] == 1:
                ret.append(i)
        return ret

    def complement_row(self, row):
        new_row = []
        for item in self.data[row]:
            if item == 0:
                new_row.append(1)
            else:
                new_row.append(0)
        self.data[row] = new_row

    def test_for_consecutive_ones(self):
        return pq_trees.PQTree.get_first_non_consecutive_ones_row(self.data)

    def rotate_first(self, row):
        self.data = self.data[row:row+1] + self.data[0:row] + self.data[row+1:]

    def find_first_non_circular_ones_row(self):
        column_to_complement = self.column_with_least_ones()
        for row in self.rows_that_have_a_one_in_col(column_to_complement):
            self.complement_row(row)

        first_bad_row = self.test_for_consecutive_ones()
        return first_bad_row

    def reduce_to_minimal_bad(self):
        first_bad_row = None
        for i in range(0, self.num_rows()):
            # print("Reducing:", self)
            first_bad_row = self.find_first_non_circular_ones_row()
            # print("First bad row:", first_bad_row)
            if first_bad_row is None:
                # Matrix is circular-ones.
                raise Exception("reduce_to_minimal_bad on circular-ones matrix")
            self.rotate_first(first_bad_row)

        # 0101
        # 1010
        # 1111 <- first_bad_row = 2
        # 0000
        # num_rows = 4
        # 4 - 2 = 2
        num_to_delete = (self.num_rows() - first_bad_row) - 1
        for i in range(num_to_delete):
            self.remove_row(first_bad_row+1)


    def __repr__(self):
        return "Matrix " + repr(self.data)

    def __str__(self):
        ret = "Matrix:\n"
        for row in self.data:
            ret += str(row) + "\n"
        return ret

def make_full_matrix(num_columns):
    ret = []
    # Add every possible row to ret
    for i in range(0, pow(2, num_columns)):
        binary = ("{0:0" + str(num_columns) + "b}").format(i)
        binary = [int(i) for i in list(binary)]
        ret.append(binary)
    return ret

def matrix_generator():
    i = 1
    while True:
        yield make_full_matrix(i)
        i += 1

def alg_1():
    for matrix in matrix_generator():
        matrix = Matrix(matrix)
        first_bad_row = None
        for j in range(0, matrix.num_rows()):
            for i in range(0, matrix.num_rows()):
                first_bad_row = matrix.find_first_non_circular_ones_row()
                if first_bad_row is None:
                    # Matrix is circular-ones.
                    continue
                matrix.rotate_first(first_bad_row)
            if first_bad_row is not None and first_bad_row > 3:
                print(matrix)
                print("first bad row is", first_bad_row)
            matrix.remove_row(0)
        print()
        print()

def make_random_row(num_columns):
    ret = []
    for i in range(0, num_columns):
        ret.append(random.getrandbits(1))
    return ret

def add_random_row_to_matrix(matrix, target_num_columns):
    # Add a random row that's not already in the matrix.
    done = False
    while not done:
        random_row = make_random_row(target_num_columns)
        if not matrix.has_row_or_inverse(random_row):
            matrix.add_row(random_row)
            done = True


def alg_2(target_num_rows, target_num_columns):
    matrix = Matrix([])
    print("Target num rows", target_num_rows)
    best_num_rows = 0
    while True:
        # Make a circular-ones matrix with target_num_rows-1 rows.
        while matrix.num_rows() < target_num_rows-1:
            # Add a random row to matrix
            add_random_row_to_matrix(matrix, target_num_columns)

            # Now, if it's not circular-ones, undo the add
            first_bad_row = matrix.find_first_non_circular_ones_row()
            if first_bad_row is not None:
                matrix.remove_row(first_bad_row)
        # Now, try to add random rows such that the resulting matrix is not circular-ones.
        # Add a random row to matrix
        add_random_row_to_matrix(matrix, target_num_columns)
        first_bad_row = matrix.find_first_non_circular_ones_row()
        if first_bad_row is not None:
            # We have a bad row.
            # We need to "minimize" the matrix by doing the Tucker alg above and repeat adding rows
            # otherwise, the resulting matrix might not be minimal

            matrix.reduce_to_minimal_bad()
            # TODO: We're still getting the reduce exception here.
            # To debug, run a bunch of times until you get the exception, and then observe
            # that the same matrix in different orders is giving incorrect "first bad row" results.
            # The "first bad row" should always be none for the same re-ordered matrix,
            # or always be non-none.
            # print(matrix)
            curr_num_rows = matrix.num_rows()
            if curr_num_rows > best_num_rows:
                best_num_rows = curr_num_rows
                print("best num rows so far", best_num_rows)
            if curr_num_rows == target_num_rows:
                print("Done!")
                print(matrix)
                return matrix
            # Remove the first row, since we know it's bad and we want to increase the size.
            matrix.remove_row(0)
if __name__ == "__main__":
    target_num_rows = 3
    target_num_columns = 4

    while True:
        matrix = alg_2(target_num_rows, target_num_columns)

        print("Verifying...")

        # If we remove any one row, is it circular? If not, it's not minimal.
        for row in range(0, matrix.num_rows()):
            import copy
            new_matrix = copy.deepcopy(matrix)
            new_matrix.remove_row(row)
            bad_row = new_matrix.find_first_non_circular_ones_row()
            if bad_row is not None:
                raise Exception("Not minimal, removed row " + str(row) + ", bad_row is " + str(bad_row))

        print("Verified")

        # TODO: lexicographically sort it, then print it again?
        # TODO: if we change any of the 1's to a zero, does it maintain the property?

        target_num_rows += 1
        target_num_columns += 1

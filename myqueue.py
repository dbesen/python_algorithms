class MyQueueNode:
    def __init__(self, item):
        self.item = item
        self.next = None

    def get(self):
        return self.item

    def __repr__(self):
        return "MyQueueNode(" + repr(self.item) + ", " + repr(self.next) + ")"

class MyQueue:
    def __init__(self):
        self.size = 0
        self.head_pointer = None
        self.tail_pointer = None

    def __len__(self):
        return self.size

    def put(self, item):
        self.size += 1
        item = MyQueueNode(item)
        if self.head_pointer is None:
            self.head_pointer = item

        if self.tail_pointer is None:
            self.tail_pointer = item
        else:
            self.tail_pointer.next = item
            self.tail_pointer = self.tail_pointer.next

    def get(self):
        if self.size == 0:
            raise Exception("Get on empty queue")
        ret = self.head_pointer.get()
        self.head_pointer = self.head_pointer.next
        self.size -= 1
        return ret

    def peek(self):
        return self.head_pointer.get()

    def __repr__(self):
        ret = "MyQueue: "
        ptr = self.head_pointer
        while ptr is not None:
            ret += repr(ptr) + ", "
            ptr = ptr.next
        return ret

import unittest
class TestQueue(unittest.TestCase):
    def test_simple_put_get(self):
        q = MyQueue()
        self.assertEquals(0, len(q))
        q.put(3)
        self.assertEquals(1, len(q))
        self.assertEquals(3, q.get())
        self.assertEquals(0, len(q))

    def test_get_when_empty(self):
        q = MyQueue()
        with self.assertRaises(Exception):
            q.get()

    def test_two_put_get(self):
        q = MyQueue()
        q.put(4)
        q.put(5)
        self.assertEquals(2, len(q))
        self.assertEquals(4, q.get())
        self.assertEquals(5, q.get())


#!/usr/bin/env python3

from functools import reduce

# Count words
def mapper(data):
    data = data.rstrip()
    parts = data.split()
    ret = {}
    for part in parts:
        if part == "":
            continue
        if part not in ret:
            ret[part] = 0
        ret[part] += 1
    return ret

def reducer(*args):
    ret = {}
    for arg in args:
        for k, v in arg.items():
            if k not in ret:
                ret[k] = 0
            ret[k] += v
    return ret

with open(__file__, "r") as f:
    data = f.readlines()

# Prove that it's parallelizable -- first we can just call it on everything
mapped = map(mapper, data)
result = reduce(reducer, mapped)
print(sorted(result.items(), key=lambda item: item[1], reverse=True))

# Second, we can call it on each line
result = {}
with open(__file__, "r") as f:
    for line in f:
        mapped = map(mapper, [line])
        result = reduce(reducer, [result, *mapped])
print(sorted(result.items(), key=lambda item: item[1], reverse=True))

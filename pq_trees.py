import unittest
from myqueue import MyQueue


class ConsecutiveSubsequences:
    """ Keep track of a set of sequences

    This class keeps track of a set of sequences.  For example, if you give it
    the sequences "a, b, c" and "b, c, d", it will remember those two lists.

    It can test whether a given sequence has all sequences stored contiguous
    with the testSequence method.  For example, if you give it the sequence
    "a, b, c, d", for the above two sequences, it will return True.  If you
    gave it the sequence "d, a, b, c", it would return False, since in that
    sequence "a, b, c" is contiguous, but "b, c, d" is not.

    """

    def __init__(self):
        """ Constructor.
        We initialize the list of sequences and the set of all nodes we've
        seen.

        The PQTree class wants a list of all nodes we've seen so it can create
        its initial configuration.

        """
        self.sequences = []
        self.all_nodes = set()

    def add(self, subseq):
        """ Add the given sequence to our list of sequences.
        """
        if subseq in self.sequences:
            # Ignore duplicate subsequence
            return

        for item in subseq:
            self.all_nodes.add(item)
        self.sequences.append(subseq)

    def addAll(self, sequences):
        """ Add all of the given sequences, after converting them to PQTreeLeaf objects.
        """
        sequences = PQTreeLeaf.make_leafs(sequences)
        for seq in sequences:
            self.add(seq)

    def addFromMatrix(self, matrix):
        # Assign numbers to each column
        out_data = []
        for row in matrix:
            out_row = []
            for (index, column) in enumerate(row):
                if column == 1:
                    out_row.append(index)
            out_data.append(out_row)

        self.addAll(out_data)

    def get_all_nodes(self):
        """ Return the set of all nodes seen.
        """
        return self.all_nodes

    def _testSingleSeq(self, needle, haystack):
        """ Private method to test if the 'needle' sequence appears
        contiguously in 'haystack'.
        """
        got_first = False
        needle = set(needle)
        for item in haystack:
            if item in needle:
                got_first = True
                needle.remove(item)
            else:
                if got_first:
                    break

        if len(needle) > 0:
            return False

        return True

    def testSequence(self, seq):
        """ Test the given sequence against all known subsequences to see if
        they appear contiguously in it.
        """

        # If the solution is None, it isn't valid.
        if seq is None:
            return False

        # Test each of our sequences against the solution.
        for sequence in self.sequences:
            if not self._testSingleSeq(sequence, seq):
                return False
        return True

    def __str__(self):
        """ Return a string representation of this class. """
        return str(self.sequences)


class TestConsecutiveSubsequences(unittest.TestCase):
    """ Unit tests for the ConsecutiveSubsequences class. """

    def assert_matches(self, seqs, testseq):
        """ Test that the given sequences (seqs) appear contiguously in testseq,
        fail if not.
        """
        cs = ConsecutiveSubsequences()
        for item in seqs:
            cs.add(item)
        self.assertTrue(cs.testSequence(testseq))

    def assert_no_match(self, seqs, testseq):
        """ Assert that at least one sequence in seqs does not appear
        contiguously in testseq.
        """
        cs = ConsecutiveSubsequences()
        for item in seqs:
            cs.add(item)
        self.assertFalse(cs.testSequence(testseq))

    def test_cs(self):
        """ Tests for the ConsecutiveSubsequences class, and in particular the
        testSequence method. """
        self.assert_matches([['a', 'b', 'c']], ['a', 'b', 'c'])
        self.assert_matches([['c', 'b', 'a']], ['a', 'b', 'c'])
        self.assert_no_match([['a', 'c']], ['a', 'b', 'c'])
        self.assert_no_match([['c', 'a']], ['a', 'b', 'c'])
        self.assert_matches([['a', 'b'], ['b', 'c']], ['a', 'b', 'c'])
        self.assert_no_match([['a', 'b'], ['a', 'c']], ['a', 'b', 'c'])


class PQTreeLeaf():
    """ Class to represent a leaf of a PQTree. """

    def __init__(self, item):
        """ Constructor. """
        self.item = item
        self.parent = None
        self.type = "l"
        self.needs_processing_count = 0

    @staticmethod
    def make_leafs(sequences):
        """ For each item in sequences, convert it to a PQTreeLeaf.

        Return a new 2d list.
        """

        leafdict = {}
        ret = []
        for items in sequences:
            ret_inner = []
            for item in items:
                if item in leafdict:
                    leaf = leafdict[item]
                else:
                    leaf = PQTreeLeaf(item)
                    leafdict[item] = leaf
                ret_inner.append(leaf)
            ret.append(ret_inner)
        return ret

    @staticmethod
    def remove_leaf_objects(sequence):
        """ Remove leaf objects from the given single sequence.
        Return a new list.
        """

        ret = []
        for item in sequence:
            ret.append(item.item)
        return ret

    def is_partial(self, seq):
        """ A leaf is never partial -- it's either in the sequence or it's not.
        """
        return False

    def is_full(self, seq):
        """ A leaf is full if it's contained in the sequence. """
        return self in seq

    def is_empty(self, seq):
        """ A leaf is empty if it's not contained in the sequence. """
        return not self.is_full(seq)

    def traverse(self):
        """ When a leaf is traversed, we simply add ourselves to the list. """
        return [self]

    def get_counts(self, seq):
        """ Return a tuple of (number of leafs in the sequence, total number of
        leafs).
        """
        # For a leaf, the counts are always either 1, 1 or 0, 1 depending on if
        # we're in the sequence.
        if self.is_full(seq):
            return (1, 1)
        else:
            return (0, 1)

    def reset_needs_processing_counts(self):
        self.needs_processing_count = 0

    def __repr__(self):
        return "PQTreeLeaf(" + repr(self.item) + ")"


class PQTreeNode():
    def __init__(self):
        raise Exception("Abstract class")

    def get_counts(self, seq):
        """ Return a tuple of (number of leafs in the sequence, total number of
        leafs).
        """
        # TODO: get_counts is inefficient, we should be calculating them in
        # bubble()
        in_count = 0
        tot_count = 0
        for child in self.children:
            if child.type == 'l':
                if child in seq:
                    in_count += 1
                tot_count += 1
            else:
                (in_child, tot_child) = child.get_counts(seq)
                in_count += in_child
                tot_count += tot_child
        return (in_count, tot_count)

    def is_full(self, seq):
        """ A node is full if all of the leafs contained in it are in the
        sequence.
        """
        (has, tot) = self.get_counts(seq)
        return has == tot

    def is_empty(self, seq):
        """ A node is empty if none of the leafs contained in it are in the
        sequence.
        """
        (has, tot) = self.get_counts(seq)
        return has == 0

    def reset_needs_processing_counts(self):
        self.needs_processing_count = 0
        for item in self.children:
            item.reset_needs_processing_counts()

    def is_partial(self, seq):
        """ A node is partial if some of its leafs are in seq and some aren't.
        """
        (in_seq, tot) = self.get_counts(seq)
        if in_seq > 0 and in_seq < tot:
            return True

    def get_partial_children(self, seq, node_type=None):
        """ Return all of our children that are partial. """
        partial_children = []
        for child in self.children:
            if child.is_partial(seq):
                if node_type is None or child.type == node_type:
                    partial_children.append(child)
        return partial_children

    def traverse(self):
        """ Traverse this node; return a list of leafs traversed. """
        ret = []
        for child in self.children:
            ret.extend(child.traverse())
        return ret

    def __repr__(self):
        """ Return a string representation of this object. """
        ret = "PQTree" + self.type.upper() + "Node(\n"
        repr_children = [indent(repr(child)) for child in self.children]
        ret += ",\n".join(repr_children)
        ret += "\n)"
        return ret


def indent(s):
    """ Method to indent a string, used for prettier output when printing a
    tree. """
    lines = s.splitlines()
    return "\n".join(["    " + item for item in lines])


class PQTreePNode(PQTreeNode):
    """ Class to represent a PQ tree P node.  A P node's children can be ordered
    arbitrarily. """

    def __init__(self):
        """ Constructor. """
        self.children = set()
        self.type = "p"
        self.parent = None
        self.needs_processing_count = 0

    def add_child(self, item):
        """ Add the given item as a child. """
        item.parent = self
        self.children.add(item)

    def remove_child(self, item):
        """ Remove the given item from our set of children. """
        self.children.remove(item)

    def replace_child(self, child, replacement):
        """ Replace the given child with the given replacement. """
        self.remove_child(child)
        self.add_child(replacement)


class PQTreeQNode(PQTreeNode):
    """ Class to represent a PQ tree Q node.  The order of a Q node's children
    may be reversed, but no other reordering can take place.
    """

    def __init__(self):
        """ Constructor. """
        self.type = "q"
        self.children = []
        self.parent = None
        self.needs_processing_count = 0

    def flip(self):
        """ Reverse our children. """
        self.children.reverse()

    def add_child(self, item):
        """ Add the given item as a child. """
        item.parent = self
        self.children.append(item)

    def replace_child(self, child, replacement):
        """ Replace the given child with the given replacement. """
        for i in range(0, len(self.children)):
            if self.children[i] == child:
                self.children[i] = replacement
                return
        raise Exception("Child not found to replace")


class template_P1():
    """ Class to represent Booth & Leucker's P1 template. """

    def __init__(self, root, seq):
        """ Constructor. Takes the root node and the sequence we're applying.
        """
        self.root = root
        self.seq = seq
        self.name = "P1"

    def applies(self):
        """ Return if this template applies to self.root. """
        if self.root.type != "p":
            return False

        (in_seq, total) = self.root.get_counts(self.seq)
        if in_seq != 0 and in_seq != total:
            return False

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        # None means do no replacement.
        return None


class template_Q1():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "Q1"

    def applies(self):
        """ Return if this template applies to self.root. """
        if self.root.type != "q":
            return False

        (in_seq, total) = self.root.get_counts(self.seq)
        if in_seq != 0 and in_seq != total:
            return False

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        return None


class template_L1():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "L1"

    def applies(self):
        """ Return if this template applies to self.root. """
        return self.root.type == 'l'

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        return None


class template_P2():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "P2"

    def applies(self):
        """ Return if this template applies to self.root. """

        # Is it a p-node?
        if self.root.type != "p":
            return False

        # Are at least two leaf children in seq?
        # Do we have no partial children?
        exists_count = 0
        for item in self.root.children:
            if item.is_partial(self.seq):
                return False
            if item.is_full(self.seq):
                exists_count += 1
        if exists_count < 2:
            return False

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        new_pnode_outer = PQTreePNode()
        new_pnode_inner = PQTreePNode()

        for child in self.root.children:
            if child.is_full(self.seq):
                new_pnode_inner.add_child(child)
            else:
                new_pnode_outer.add_child(child)

        new_pnode_outer.add_child(new_pnode_inner)

        return new_pnode_outer


class template_P3():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "P3"

    def applies(self):
        """ Return if this template applies to self.root. """
        # Is it a p-node?
        if self.root.type != "p":
            return False

        # Is there at least one leaf child in seq and one not in seq?
        # Also, there shouldn't be any partial children.
        exists_count = 0
        not_exists = 0
        for item in self.root.children:
            if item.is_partial(self.seq):
                return False
            if item.is_full(self.seq):
                exists_count += 1
            else:
                not_exists += 1
        if exists_count > 0 and not_exists > 0:
            return True

        return False

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        # Separate children into those in seq and those not in seq
        in_seq = PQTreePNode()
        not_in_seq = PQTreePNode()
        for item in self.root.children:
            if item.is_full(self.seq):
                in_seq.add_child(item)
            else:
                not_in_seq.add_child(item)

        # If there is only one child on a side, we don't need the p node
        if len(in_seq.children) == 1:
            in_seq = in_seq.children.pop()
        if len(not_in_seq.children) == 1:
            not_in_seq = not_in_seq.children.pop()

        new_q_node = PQTreeQNode()
        new_q_node.add_child(not_in_seq)
        new_q_node.add_child(in_seq)

        return new_q_node


class template_P5():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "P5"

    def applies(self):
        """ Return if this template applies to self.root. """
        # Are we a p-node?
        if self.root.type != "p":
            return False

        # Do we have exactly one partial q-node?
        partial_children = self.root.get_partial_children(self.seq, "q")
        if len(partial_children) != 1:
            return False
        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        full_children = PQTreePNode()
        empty_children = PQTreePNode()
        for child in self.root.children:
            if child.is_partial(self.seq):
                partial_child = child
            elif child.is_full(self.seq):
                full_children.add_child(child)
            else:
                empty_children.add_child(child)

        new_q = PQTreeQNode()
        if len(empty_children.children) == 1:
            new_q.add_child(empty_children.children.pop())
        elif len(empty_children.children) > 1:
            new_q.add_child(empty_children)

        # Orient partial_child
        if partial_child.children[0].is_full(self.seq):
            partial_child.flip()

        # We don't want to add partial_child as a direct child of new_q here
        # since that will result in a q-node child of a q-node, which is illegal.
        # So we add all its children.  See the diagram in the B&L paper on p. 347.
        for item in partial_child.children:
            new_q.add_child(item)

        if len(full_children.children) == 1:
            new_q.add_child(full_children.children.pop())
        elif len(full_children.children) > 1:
            new_q.add_child(full_children)

        return new_q


class template_P6():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "P6"

    def applies(self):
        """ Return if this template applies to self.root. """
        # Are we a p-node?
        if self.root.type != "p":
            return False

        # Do we have exactly two partial children?
        count = 0
        for child in self.root.children:
            if child.is_partial(self.seq):
                count += 1
        if count != 2:
            return False
        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        new_marked_p_node = PQTreePNode()
        new_q = PQTreeQNode()
        partial_children = []
        to_remove = []
        # TODO: we shouldn't iterate over all children ever...
        for child in self.root.children:
            if child.is_partial(self.seq):
                to_remove.append(child)
                partial_children.append(child)
            if child.is_full(self.seq):
                to_remove.append(child)
                new_marked_p_node.add_child(child)

        for child in to_remove:
            self.root.remove_child(child)

        # Orient partial children
        if partial_children[0].children[0].is_full(self.seq):
            partial_children[0].flip()

        if not partial_children[1].children[0].is_full(self.seq):
            partial_children[1].flip()

        for child in partial_children[0].children:
            new_q.add_child(child)
        if len(new_marked_p_node.children) > 0:
            new_q.add_child(new_marked_p_node)
        for child in partial_children[1].children:
            new_q.add_child(child)

        self.root.add_child(new_q)


class template_Q3():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "Q3"

    def applies(self):
        """ Return if this template applies to self.root. """
        # Are we a q-node?
        if self.root.type != "q":
            return False

        # Is neither end-child full?
        if (self.root.children[0].is_full(self.seq) or
                self.root.children[-1].is_full(self.seq)):
            return False

        # Note that we don't need to be exactly doubly-partial here, since we could be missing one of the children
        # and the template still matches.  Q2 only matches if at least one end-child is full.

        # Now, we have to check if our child order is valid.
        # We could have (for example) a full node, followed by an empty node, followed by another full node.
        # That would mean we can't match.

        # We must be: empty, partial, full, partial, empty.
        # Any of the above could be missing.
        # We must have a maximum of 2 partial nodes.

        # Phases (in regex-like syntax):
        # (empty)* (partial)? (full)* (partial)? (empty)*
        #   1          2        3         4         5

        # This table tells us which phase to go to given a particular child type.
        # Phase 0 means no match.
        #                In phase: 1  2  3  4  5
        state_transition_table = [[1, 5, 5, 5, 5 ], # Empty
                                  [3, 3, 3, 0, 0 ], # Full
                                  [2, 4, 4, 0, 0 ], # Partial
                                 ]
        phase = 1

        for child in self.root.children:
            counts = child.get_counts(self.seq)
            if counts[0] == 0:
                row = 0 # empty
            elif counts[0] == counts[1]:
                row = 1 # full
            else:
                row = 2 # partial

            phase = state_transition_table[row][phase-1]
            if phase == 0:
                return False # No match

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """

        # Outer: P, F, E
        # Inner: E, P, F

        new_root = PQTreeQNode()
        found_non_empty = False
        # TODO: we shouldn't iterate over all children ever...
        for child in self.root.children:
            if child.is_full(self.seq):
                found_non_empty = True
            if child.is_partial(self.seq):
                # Orient child
                if not found_non_empty:
                    found_non_empty = True
                    # Left child
                    if (child.children[0].is_full(self.seq) or
                            child.children[-1].is_empty(self.seq)):
                        child.flip()
                else:
                    # Right child
                    if (child.children[0].is_empty or
                            child.children[-1].is_full(self.seq)):
                        child.flip()
                for grandchild in child.children:
                    new_root.add_child(grandchild)
            else:
                new_root.add_child(child)

        return new_root


class template_Q2():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "Q2"

    def applies(self):
        """ Return if this template applies to self.root. """
        # Is it a q-node?
        if self.root.type != "q":
            return False

        # Do we have at most one child that's a partial q node?
        count_partial = 0
        for item in self.root.children:
            if item.type == "q":
                if item.is_partial(self.seq):
                    count_partial += 1

        if count_partial > 1:
            return False

        # If we only have one partial child here, we match.
        if len(self.root.children) == 1:
            return True

        flipped_root = False
        # Orient root to make testing easier
        if (self.root.children[0].is_full(self.seq) or
                self.root.children[-1].is_empty(self.seq)):
            flipped_root = True
            self.root.flip()

        # They must all be empty, until the optional partial child, then they
        # must all be full.
        past_empty = False
        for child in self.root.children:
            if not past_empty:
                if not child.is_empty(self.seq):
                    past_empty = True
            else:
                if not child.is_full(self.seq):
                    # Unflip root
                    if flipped_root:
                        self.root.flip()
                    return False  # Q3

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        # Root was oriented by applies()
        new_q_node = PQTreeQNode()
        for child in self.root.children:
            if child.type != "q" or not child.is_partial(self.seq):
                new_q_node.add_child(child)
            else:
                # Orient child
                if (child.children[0].is_full(self.seq) or
                        child.children[-1].is_empty(self.seq)):
                    child.flip()
                for grandchild in child.children:
                    new_q_node.add_child(grandchild)
        return new_q_node


class template_P4():
    def __init__(self, root, seq):
        """ Constructor. """
        self.root = root
        self.seq = seq
        self.name = "P4"

    def applies(self):
        """ Return if this template applies to self.root. """
        if self.root.type != "p":
            return False

        partial_children = self.root.get_partial_children(self.seq, "q")
        if len(partial_children) != 1:
            return False

        if partial_children[0].type != 'q':
            return False

        return True

    def get_replacement(self):
        """ Return the replacement if this template applies. """
        new_p = PQTreePNode()
        self.partial_child = self.root.get_partial_children(self.seq, "q")[0]

        # TODO: possible problem: the q node may be backwards here (i.e. marked
        # children on the left).
        # So it may be necessary to reverse the q node here.
        to_remove = []
        for child in self.root.children:
            if child.is_full(self.seq):
                new_p.add_child(child)
                to_remove.append(child)

        for child in to_remove:
            self.root.remove_child(child)

        if len(new_p.children) == 1:
            self.partial_child.add_child(new_p.children.pop())
        else:
            self.partial_child.add_child(new_p)

        return None  # None means don't do the root replacement


class PQTree():
    """ Class to represent a Booth & Leucker PQ tree. """

    def __init__(self):
        """ Constructor. """
        # A PQ tree starts as a single P node.
        self.root = PQTreePNode()

    @staticmethod
    def get_first_non_consecutive_ones_row(matrix):
        """ Return the first row in matrix that prevents it from being consecutive-ones.  Rows are zero-indexed. """
        # Make our tree.
        t = PQTree()

        # Make our set of sequences.
        cs = ConsecutiveSubsequences()

        # Add our sequences to cs.
        cs.addFromMatrix(matrix)

        # Execute the algorithm.
        solution = t.solve(cs)

        if solution is None:
            return t.last_sequence_number
        else:
            # In this case, None means success, since we don't have a first bad row.
            return None

    @staticmethod
    def is_consecutive_ones(matrix):
        """ Return True if matrix is consecutive-ones, false otherwise. """
        fbr = PQTree.get_first_non_consecutive_ones_row(matrix)
        return fbr == None

    def solve(self, cs):
        """ Find one solution to the given set of sequences.
        The cs argument is a ConsecutiveSubsequence class, representing
        a set of sequences that should each be consecutive in our final
        solution.
        """
        # First, we add all the nodes in all the sequences to our root P-node.
        for node in cs.get_all_nodes():
            self.root.add_child(node)

        self.total_num_leafs = len(self.root.children)

        # Then, for each sequence, we "add" it, making changes to the tree as
        # we proceed.
        sequence_number = -1
        for sequence in cs.sequences:
            sequence_number += 1
            try:
                self.add_seq(sequence)
            except Exception as e:
                if e.args[0] == "No templates matched":
                    # TODO: use type instead of message
                    # If no template matched, that means the set of sequences
                    # is unsolvable.
                    self.last_sequence_number = sequence_number
                    return None
                else:
                    raise

        # Now, our tree is fully built, and any traversal of the tree will be a
        # solution.
        return self.traverse()

    def bubble(self, seq):
        """ Perform the bubble phase of Booth & Leucker. """
        # TODO: This bubble phase is not complete -- I'm still doing some
        # processing during the reduce phase that should be done during bubble.
        # See the other TODOs.
        self.root.reset_needs_processing_counts()
        queue = MyQueue()
        queued = set()  # so we don't double-queue things
        for item in seq:
            queue.put(item)
            queued.add(item)
            item.needs_processing_count = 1
        while len(queue) > 0:
            item = queue.get()
            if item.parent is not None:
                item.parent.needs_processing_count += 1
                if item.parent not in queued:
                    queue.put(item.parent)
                    queued.add(item.parent)

    def add_seq(self, seq):
        """ Add the given sequence.  Per Booth & Leucker, this is a 2-phase
        process.  First we bubble, and then we reduce.
        """

        # If the sequence contains all the items in the tree, we can ignore it.
        if len(seq) == self.total_num_leafs:
            return

        self.bubble(seq)
        self.reduce(seq)

    def reduce(self, seq):
        """ Perform a Booth & Leucker reduction.
        This is implemented in a similar fashion to the way the algorithm is
        presented by B&L, with a few minor changes for readability and
        simplicity.
        """

        do_print = False
        if do_print:
            print()
            print()
            print("=" * 20)
            print("Adding seq", seq)
        seq = set(seq)  # since we will be removing from it by item later

        queue = MyQueue()
        for item in seq:
            queue.put(item)
        while len(queue) > 0:
            item = queue.get()

            (in_seq, total) = item.get_counts(seq)
            if do_print:
                print()
                print("Processing item:")
                print(item)
            if in_seq == len(seq):  # item is root(t, s)
                templates = [
                    template_L1,
                    template_P1,
                    template_P2,
                    template_P4,
                    template_P6,
                    template_Q1,
                    template_Q2,
                    template_Q3,
                ]
            else:  # item is not root(t, s)
                if item.parent is not None:
                    item.parent.needs_processing_count -= 1
                    if item.parent.needs_processing_count == 0:
                        queue.put(item.parent)
                templates = [
                    template_L1,
                    template_P1,
                    template_P3,
                    template_P5,
                    template_Q1,
                    template_Q2,
                ]
            found_match = False
            for template in templates:
                template = template(item, seq)
                if template.applies():
                    if do_print:
                        print(template.name, "applies")
                    replacement = template.get_replacement()
                    if replacement is not None:
                        replacement.parent = item.parent
                        if item == self.root:
                            self.root = replacement
                        else:
                            item.parent.replace_child(item, replacement)
                    found_match = True
                    if do_print:
                        print("after applying template", template.name, ":")
                        print(self.root)
                    break
                else:
                    if do_print:
                        print(template.name, "does not apply")

            if not found_match:
                if do_print:
                    print("Raising exception no templates matched")
                raise Exception("No templates matched")

    def traverse(self):
        """ Traverse the tree, returning a list of its leafs. """
        return self.root.traverse()


class TestPQTree(unittest.TestCase):
    """ Test class for PQTree. """

    def assert_solvable(self, sequences):
        """ Assert that the given set of sequences is solvable. """
        # Make our tree.
        t = PQTree()

        # Make our set of sequences.
        cs = ConsecutiveSubsequences()

        # Add our sequences to cs.
        cs.addAll(sequences)

        # Execute the algorithm.
        solution = t.solve(cs)
        # print()
        # print()
        # print("=" * 30)
        # print("solution", solution)

        # Test that our solution is valid.
        self.assertTrue(cs.testSequence(solution))

    def assert_not_solvable(self, sequences):
        """ Assert that the given set of sequences is not solvable. """
        # Make our tree.
        t = PQTree()

        # Make our set of sequences.
        cs = ConsecutiveSubsequences()

        # Add our sequences to cs.
        cs.addAll(sequences)

        # Execute the algorithm.
        solution = t.solve(cs)

        # Assert that we weren't able to find a solution.
        self.assertIs(None, solution)

    def assert_is_consecutive_ones(self, matrix):
        """ Assert that the given matrix is consecutive-ones. """
        self.assertTrue(PQTree.is_consecutive_ones(matrix))

    def assert_is_not_consecutive_ones(self, matrix):
        """ Assert that the given matrix is not consecutive-ones. """
        self.assertFalse(PQTree.is_consecutive_ones(matrix))

    def test_solve(self):
        """ Test the solve method with various sets of sequences. """

        self.assert_solvable([['a', 'b']])

        # Overlapping sequences
        self.assert_solvable([['a', 'b', 'c'], ['a', 'b'], ['d', 'e']])

        self.assert_solvable([['a', 'b'], ['b', 'c']])
        self.assert_solvable([['a', 'b', 'c'], ['d', 'c']])
        self.assert_solvable([['a', 'b'], ['c', 'd']])
        self.assert_not_solvable([['a', 'b'], ['b', 'c'], ['c', 'a']])
        self.assert_solvable([['a', 'b', 'c'], ['b', 'c', 'd']])

        # This matches P6.
        self.assert_solvable([['a', 'b'], ['b', 'c'], ['f', 'g'], [
                          'g', 'h'], ['c', 'd', 'e', 'f']])

        # Q3
        self.assert_solvable([
            ['a', 'b'],
            ['c', 'd'], ['d', 'e'],
            ['f', 'g'],
            ['h', 'i'], ['i', 'j'],
            ['k', 'l'],
            ['b', 'c'],
            ['j', 'k'],
            ['e', 'f'],
            ['g', 'h'],
            ['e', 'f', 'g', 'h']
        ])

        # P6 again
        self.assert_solvable([['a', 'b'], ['c', 'd'], ['d', 'e'], [
                          'f', 'g'], ['h', 'i'], ['e', 'f', 'g', 'h']])

        # P5
        self.assert_solvable([
            ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
            ['a', 'b'],
            ['c', 'd'], ['d', 'e'],
            ['f', 'g'],
            ['h', 'i'],
            ['e', 'f', 'g', 'h']
        ])

        # Other tests
        self.assert_solvable([
            ['a', 'b'],
            ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
            ['c', 'd'], ['d', 'e'],
            ['f', 'g'],
            ['h', 'i'],
            ['e', 'f', 'g', 'h']
        ])

        self.assert_solvable([
            ['a', 'b'],
            ['b', 'c', 'd', 'e', 'f', 'g', 'h'],
            ['c', 'd'], ['d', 'e'],
            ['f', 'g'],
            ['h', 'i'],
            ['e', 'f', 'g', 'h']
        ])

        self.assert_solvable([
            ['b', 'c', 'd', 'e', 'f', 'g', 'h'],
            ['a', 'b'],
            ['c', 'd'], ['d', 'e'],
            ['f', 'g'],
            ['h', 'i'],
            ['e', 'f', 'g', 'h']
        ])

    def test_assert_ones_can_fail(self):
        with self.assertRaises(AssertionError):
            self.assert_is_consecutive_ones([
                [0, 1, 1],
                [1, 1, 0],
                [1, 0, 1]
            ])

        with self.assertRaises(AssertionError):
            self.assert_is_not_consecutive_ones([
                [0, 1, 1, 0],
                [1, 1, 0, 0]
            ])

    def test_ones(self):
        self.assert_is_consecutive_ones([
            [0, 1, 1, 0],
            [1, 1, 0, 0]
        ])

        self.assert_is_not_consecutive_ones([
            [0, 1, 1],
            [1, 1, 0],
            [1, 0, 1]
        ])

if __name__ == "__main__":
    """ Example of how to execute the code to obtain a solution. """
    sequences = [
            ['a', 'b', 'c'],
            ['b', 'c', 'd']
            ]

    # Make our tree.
    t = PQTree()

    # Make our set of sequences.
    cs = ConsecutiveSubsequences()

    # Add our sequences to cs.
    cs.addAll(sequences)

    # Execute the algorithm.
    solution = t.solve(cs)

    # Remove PQTreeLeaf objects from the solution
    solution = PQTreeLeaf.remove_leaf_objects(solution)

    # print()
    # print()
    # print("Solution:")
    # print(solution)
